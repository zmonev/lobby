<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $table = 'game';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function provider()
    {
        return $this->belongsTo('App\GameProvider', 'game_provider_id', 'id');
    }
    public function type()
    {
        return $this->belongsTo('App\GameType', 'game_type_id', 'id');
    }
}
