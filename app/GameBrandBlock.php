<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameBrandBlock extends Model
{
    protected $table = 'game_brand_block';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
}
