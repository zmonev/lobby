<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameCountryBlock extends Model
{
    protected $table = 'game_country_block';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
}
