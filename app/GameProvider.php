<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameProvider extends Model
{
    protected $table = 'game_providers';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
}
