<?php

namespace App\Http\Controllers\Api;

use App\Brand;
use Illuminate\Routing\Controller as BaseController;

class BrandController extends BaseController
{
    public function getAllBrands()
    {
        return response()->json(['brands' => Brand::where('enabled', 1)->get()]);
    }
}
