<?php

namespace App\Http\Controllers\Api;

use App\Country;
use Illuminate\Routing\Controller as BaseController;

class CountryController extends BaseController
{
    public function getAllCountries()
    {
        return response()->json(['countries' => Country::orderBy('country', 'ASC')->get()]);
    }
}
