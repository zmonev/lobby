<?php

namespace App\Http\Controllers\Api;

use App\Brand;
use App\Country;
use App\Game;
use App\GameBrandBlock;
use App\GameCountryBlock;
use App\GameType;
use Illuminate\Routing\Controller as BaseController;

class GameController extends BaseController
{
    public function getGamesForBrandAndCountry($brandId, $countryCode, $typeId)
    {
        $blockedBrandsCodes = [];
        $blockedCountriesCodes = [];
        if($blockedBrands = GameBrandBlock::where('brandid', $brandId)->get()) {
            foreach ($blockedBrands as $blockedBrand) {
                $blockedBrandsCodes[] = $blockedBrand->launchcode;
            }
        }
        if($blockedCountries = GameCountryBlock::where('country', $countryCode)->get()) {
            foreach ($blockedCountries as $blockedCountry) {
                $blockedCountriesCodes[] = $blockedCountry->launchcode;
            }
        }

        $builder = Game::with('provider')->with('type')->orderBy('rtp', 'desc');
        if (!empty($blockedBrandsCodes)) {
            $builder->whereNotIn('launchcode', $blockedBrandsCodes);
        }
        if (!empty($blockedCountriesCodes)) {
            $builder->whereNotIn('launchcode', $blockedCountriesCodes);
        }
        if($typeId != 0) {
            $builder->where('game_type_id', $typeId);
        }
        $games = $builder->get();

        return response()->json(['games' => $games]);
    }
}
