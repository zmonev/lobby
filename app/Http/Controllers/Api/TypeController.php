<?php

namespace App\Http\Controllers\Api;

use App\Brand;
use App\GameType;
use Illuminate\Routing\Controller as BaseController;

class TypeController extends BaseController
{
    public function getAllTypes()
    {
        return response()->json(['types' => GameType::orderBy('name', 'ASC')->get()]);
    }
}
