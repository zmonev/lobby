$(document).ready(function() {
    $('#loader').show();
    getTheData();
    $('#brandSelect').off('change').on('change', function (e) {
        getGames($(this).val(), $( "#countrySelect option:selected").val(), $( "#typeSelect option:selected").val());
    });

    $('#countrySelect').off('change').on('change', function (e) {
        getGames($( "#brandSelect option:selected").val(), $(this).val(), $( "#typeSelect option:selected").val());
    });

    $('#typeSelect').off('change').on('change', function (e) {
        getGames($( "#brandSelect option:selected").val(), $( "#countrySelect option:selected").val(), $(this).val());
    });
});

function getTheData() {
    $.ajax({
        type: 'GET',
        url: 'api/brands',
        dataType: "json",
        async:true,
        success: function(data) {
            var brands = data['brands'];
            var brandsSelect = $('#brandSelect');
            brandsSelect.empty();
            for (var i = 0; i < brands.length; i++) {
                brandsSelect.append('<option value=' + brands[i].id + '>' + brands[i].brand + '</option>');
            }
            getCountries();
        }
    });
}

function getCountries() {
    $.ajax({
        type: 'GET',
        url: 'api/countries',
        dataType: "json",
        async:true,
        success: function(data) {
            var countries = data['countries'];
            var countrySelect = $('#countrySelect');
            countrySelect.empty();
            for (var i = 0; i < countries.length; i++) {
                countrySelect.append('<option value=' + countries[i].code + '>' + countries[i].country + '</option>');
            }
            getTypes();
        }
    });
}

function getTypes() {
    $.ajax({
        type: 'GET',
        url: 'api/types',
        dataType: "json",
        async:true,
        success: function(data) {
            var types = data['types'];
            var typeSelect = $('#typeSelect');
            typeSelect.empty();
            typeSelect.append('<option value=0>All</option>');
            for (var i = 0; i < types.length; i++) {
                typeSelect.append('<option value=' + types[i].id + '>' + types[i].name + '</option>');
            }
            getGames($( "#brandSelect option:selected" ).val(), $( "#countrySelect option:selected" ).val(), $( "#typeSelect option:selected" ).val());
        }
    });
}

function getGames(brandId, countryCode, typeId) {
    $('#loader').show();
    var gamesContainer = $('#gamesGallery');
    gamesContainer.empty();
    $.ajax({
        type: 'GET',
        url: 'api/games/' + brandId + '/' + countryCode + '/' + typeId,
        dataType: "json",
        async:true,
        success: function(data) {
            var games = data['games'];


            for (var i = 0; i < games.length; i++) {
                gamesContainer.append(`
            <div class="col-lg-4 col-md-6 mb-4">
                    <div class="card h-100">
                        <a href="#" class="openLaunchCode" data-launch-code="${games[i].launchcode}"><img class="card-img-top" src="https://stage.whgstage.com/scontent/images/games/${games[i].launchcode}.jpg" alt=""></a>
                        <div class="card-body">
                            <h4 class="card-title">
                                <a href="#" class="openLaunchCode" data-launch-code="${games[i].launchcode}">${games[i].name}</a>
                            </h4>
                            <h6>Provider: ${games[i].provider.name}</h6>
                            <h6>Type: ${games[i].type.name}</h6>
                            <h6>RTP: ${(games[i].rtp) ? games[i].rtp : 'N/A'}</h6>
                            <h6>Mobile: ${(games[i].mobile == 1) ? 'YES' : 'NO'}</h6>
                        </div>
                    </div>
                </div>
            </div>
                `);
            }
            gamesContainer.show();
            $('.openLaunchCode', '#gamesGallery').off('click').on('click', function (e) {
                e.preventDefault();
                alert($(this).attr('data-launch-code'))
            });
            $('#loader').hide();
        }
    });
}
