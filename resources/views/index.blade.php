<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <style>
        .loader {
            border-top: 16px solid blue;
            border-right: 16px solid green;
            border-bottom: 16px solid red;
            border-left: 16px solid pink;
            border-radius: 50%;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
            margin: auto;
        }

        /* Safari */
        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
    </style>
    <title>Lobby</title>

    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="css/lobby.css" rel="stylesheet">

</head>

<body>
<br />
<br />
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#">Lobby</a>

    </div>
</nav>

<div class="container" id="container">

    <div class="row">

        <div class="col-lg-3">

            <h1 class="my-4">Brand</h1>
            <div class="list-group">
                <select name="brand" id="brandSelect">
                </select>
            </div>
            <h1 class="my-4">Country</h1>
            <div class="list-group">
                <select name="country" id="countrySelect">
                </select>
            </div>
            <h1 class="my-4">Game Type</h1>
            <div class="list-group">
                <select name="types" id="typeSelect">
                </select>
            </div>
        </div>



        <div class="col-lg-9">
            <div class="loader" id="loader" style="display: none;"></div>
            <div class="row" id="gamesGallery" style="display: none;"></div>

        </div>


    </div>

</div>

<br />
<br />
<br />

<footer class="py-5 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; White Hat Gaming</p>
    </div>

</footer>

    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="js/lobby.js"></script>
</body>

</html>
