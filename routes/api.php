<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/brands', 'Api\BrandController@getAllBrands');
Route::get('/countries', 'Api\CountryController@getAllCountries');
Route::get('/types', 'Api\TypeController@getAllTypes');
Route::get('/games/{brandId}/{countryCode}/{typeId}', 'Api\GameController@getGamesForBrandAndCountry')->where('brandId', '[0-9]+')->where('countryCode', '[A-Z]+')->where('type', '[0-9]+');
