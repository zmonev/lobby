<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BrandsTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testFetchingTheBrands()
    {
        $response = $this->json('GET', '/api/brands');

        $response
            ->assertStatus(200)
            ->assertJson([
                'brands' => [],
            ]);
    }
}
