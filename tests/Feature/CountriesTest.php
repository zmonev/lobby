<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CountriesTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testFetchingTheCountries()
    {
        $response = $this->json('GET', '/api/countries');

        $response
            ->assertStatus(200)
            ->assertJson([
                'countries' => [],
            ]);
    }
}
