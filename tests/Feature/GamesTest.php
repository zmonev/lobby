<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GamesTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testFetchingTheGamesWithGivenFilters()
    {
        $brandsResponse = $this->get('/api/brands');
        $brandsResponse = json_decode($brandsResponse->content(), 1);
        $countriesResponse = $this->get('/api/countries');
        $countriesResponse = json_decode($countriesResponse->content(), 1);
        $typesResponse = $this->get('/api/types');
        $typesResponse = json_decode($typesResponse->content(), 1);
        $response = $this->json('GET', '/api/games/' . $brandsResponse['brands'][0]['id'] . '/' . $countriesResponse['countries'][0]['code'] . '/' . $typesResponse['types'][0]['id']);
        $response
            ->assertStatus(200)
            ->assertJson([
                'games' => [],
            ]);
    }
}
