<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TypesTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testFetchingTheTypes()
    {
        $response = $this->json('GET', '/api/types');

        $response
            ->assertStatus(200)
            ->assertJson([
                'types' => [],
            ]);
    }
}
